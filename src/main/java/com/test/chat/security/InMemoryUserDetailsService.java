package com.test.chat.security;

import com.test.chat.domain.User;
import com.test.chat.service.UserStorage;
import lombok.AllArgsConstructor;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Component;

import java.util.Collections;

@Component
@AllArgsConstructor
public class InMemoryUserDetailsService implements UserDetailsService {

    private UserStorage userStorage;

    @Override
    public UserDetails loadUserByUsername(String s) throws UsernameNotFoundException {
        String login = s.toLowerCase();
        User userByUsername = userStorage.findUserByUsername(login);
        if (userByUsername == null) {
            throw new UsernameNotFoundException("User " + login + " was not found!");
        }
        return new org.springframework.security.core.userdetails.User(login, userByUsername.getPassword(), Collections.emptyList());
    }
}
