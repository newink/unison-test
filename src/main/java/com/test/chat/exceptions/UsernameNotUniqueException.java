package com.test.chat.exceptions;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@AllArgsConstructor
public class UsernameNotUniqueException extends Exception {

    private String username;
}
