package com.test.chat.web.ws;

import com.test.chat.domain.Message;
import com.test.chat.service.ChatService;
import lombok.AllArgsConstructor;
import org.springframework.messaging.handler.annotation.MessageMapping;
import org.springframework.stereotype.Controller;

@Controller
@AllArgsConstructor
public class MessagesController {

    private final ChatService chatService;

    @MessageMapping("/chat/send")
    public void onRecievedMessage(Message message) {
        chatService.sendMessage(message);
    }
}
