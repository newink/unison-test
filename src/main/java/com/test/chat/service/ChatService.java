package com.test.chat.service;

import com.test.chat.domain.Message;
import lombok.AllArgsConstructor;
import org.springframework.messaging.simp.SimpMessagingTemplate;
import org.springframework.stereotype.Service;

import java.time.LocalDateTime;
import java.util.Collection;
import java.util.Queue;
import java.util.concurrent.ConcurrentLinkedQueue;

@Service
@AllArgsConstructor
public class ChatService {

    private final Queue<Message> messageStorage = new ConcurrentLinkedQueue<>();

    private final SimpMessagingTemplate messagingTemplate;

    {
        messageStorage.add(new Message("username", "sdfh", LocalDateTime.now()));
        messageStorage.add(new Message("asd", "zxc", LocalDateTime.now()));
        messageStorage.add(new Message("asd", "fdg", LocalDateTime.now()));
        messageStorage.add(new Message("username", "sfgthj", LocalDateTime.now()));
    }

    public void sendMessage(Message message) {
        messagingTemplate.convertAndSend("/chat", message);
        messageStorage.add(message);
    }

    public Collection<Message> getMessageHistory() {
        return messageStorage;
    }
}
