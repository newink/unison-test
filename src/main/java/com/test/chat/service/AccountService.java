package com.test.chat.service;

import com.test.chat.domain.User;
import com.test.chat.exceptions.UsernameNotUniqueException;
import com.test.chat.security.jwt.JWTToken;
import com.test.chat.security.jwt.TokenProvider;
import com.test.chat.web.rest.dto.LoginDTO;
import lombok.AllArgsConstructor;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContext;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.stereotype.Service;

@Service
@AllArgsConstructor
public class AccountService {

    private final UserStorage userStorage;

    private final AuthenticationManager authenticationManager;

    private final TokenProvider tokenProvider;

    public void registerUser(User user) throws UsernameNotUniqueException {
        if (userStorage.isUserPresent(user.getUsername())) {
            throw new UsernameNotUniqueException(user.getUsername());
        }
        userStorage.addUser(user);
    }

    public User getCurrentUser() {
        SecurityContext securityContext = SecurityContextHolder.getContext();
        Authentication authentication = securityContext.getAuthentication();
        UserDetails springSecurityUser;
        String username = null;

        if (authentication != null) {
            if (authentication.getPrincipal() instanceof UserDetails) {
                springSecurityUser = (UserDetails) authentication.getPrincipal();
                username = springSecurityUser.getUsername();
            } else if (authentication.getPrincipal() instanceof String) {
                username = (String) authentication.getPrincipal();
            }
        }

        return userStorage.findUserByUsername(username);
    }

    public JWTToken loginUser(LoginDTO loginDTO) {
        UsernamePasswordAuthenticationToken authenticationToken =
                new UsernamePasswordAuthenticationToken(loginDTO.getUsername(), loginDTO.getPassword());
        Authentication authentication = authenticationManager.authenticate(authenticationToken);
        SecurityContextHolder.getContext().setAuthentication(authentication);
        String token = tokenProvider.createToken(authentication);
        return new JWTToken(token);
    }
}
