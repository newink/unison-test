package com.test.chat.websockets;

import com.test.chat.security.jwt.TokenProvider;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.context.ApplicationListener;
import org.springframework.messaging.Message;
import org.springframework.messaging.simp.SimpMessagingTemplate;
import org.springframework.messaging.simp.stomp.StompHeaderAccessor;
import org.springframework.messaging.support.MessageHeaderAccessor;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.stereotype.Component;
import org.springframework.util.StringUtils;
import org.springframework.web.socket.messaging.SessionConnectEvent;

import java.time.LocalDateTime;

@Slf4j
@Component
@AllArgsConstructor
public class ConnectEvent implements ApplicationListener<SessionConnectEvent> {

    private final SimpMessagingTemplate simpMessagingTemplate;

    private final TokenProvider tokenProvider;

    public void onApplicationEvent(SessionConnectEvent event) {

        Message<?> message = event.getMessage();
        String jwt = resolveToken(message);
        if (StringUtils.hasText(jwt) && this.tokenProvider.validateToken(jwt)) {
            Authentication authentication = this.tokenProvider.getAuthentication(jwt);
            SecurityContextHolder.getContext().setAuthentication(authentication);
            String username = "";
            if (authentication != null) {
                if (authentication.getPrincipal() instanceof UserDetails) {
                    username = ((UserDetails) authentication.getPrincipal()).getUsername();
                } else if (authentication.getPrincipal() instanceof String) {
                    username = (String) authentication.getPrincipal();
                }
            }
            simpMessagingTemplate.convertAndSend("/chat", new com.test.chat.domain.Message(username, "User connected", LocalDateTime.now()));
        }
    }

    private String resolveToken(org.springframework.messaging.Message<?> message) {
        StompHeaderAccessor accessor = MessageHeaderAccessor.getAccessor(message, StompHeaderAccessor.class);
        String bearerToken = accessor.getFirstNativeHeader("Authorization");
        if (StringUtils.hasText(bearerToken) && bearerToken.startsWith("Bearer ")) {
            return bearerToken.substring(7, bearerToken.length());
        }
        return null;
    }
}