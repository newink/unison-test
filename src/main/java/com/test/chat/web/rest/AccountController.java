package com.test.chat.web.rest;

import com.test.chat.domain.User;
import com.test.chat.security.jwt.JWTToken;
import com.test.chat.service.AccountService;
import com.test.chat.web.rest.dto.LoginDTO;
import lombok.AllArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

@AllArgsConstructor
@RestController
@RequestMapping("/account")
public class AccountController {

    private final AccountService accountService;

    @PostMapping("/register")
    @ResponseStatus(HttpStatus.OK)
    public ResponseEntity<JWTToken> registerUser(@Valid @RequestBody User user) throws Exception {
        accountService.registerUser(user);
        JWTToken jwtToken = accountService.loginUser(new LoginDTO(user.getUsername(), user.getPassword()));
        return new ResponseEntity<>(jwtToken, HttpStatus.OK);
    }

    @PostMapping("/login")
    public ResponseEntity<JWTToken> login(@Valid @RequestBody LoginDTO loginDTO) {
        JWTToken jwtToken = accountService.loginUser(loginDTO);
        return new ResponseEntity<>(jwtToken, HttpStatus.OK);
    }

    @GetMapping("/info")
    public ResponseEntity<User> getUserInfo() {
        return new ResponseEntity<>(accountService.getCurrentUser(), HttpStatus.OK);
    }
}
