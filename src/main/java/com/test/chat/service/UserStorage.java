package com.test.chat.service;

import com.test.chat.domain.User;
import org.springframework.stereotype.Service;

import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

@Service
public class UserStorage {

    private final Map<String, User> userStorage = new ConcurrentHashMap<>();

    public void addUser(User user) {
        userStorage.put(user.getUsername(), user);
    }

    public boolean isUserPresent(String username) {
        return userStorage.containsKey(username);
    }

    public User findUserByUsername(String username) {
        return userStorage.get(username);
    }
}
