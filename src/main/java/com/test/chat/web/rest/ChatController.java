package com.test.chat.web.rest;

import com.test.chat.domain.Message;
import com.test.chat.service.ChatService;
import lombok.AllArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.Collection;

@RestController
@AllArgsConstructor
public class ChatController {

    private final ChatService chatService;

    @GetMapping("/chat/history")
    public ResponseEntity<Collection<Message>> getChatHistory() {
        return new ResponseEntity<>(chatService.getMessageHistory(), HttpStatus.OK);
    }
}
