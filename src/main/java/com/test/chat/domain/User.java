package com.test.chat.domain;

import lombok.Data;

import java.time.LocalDate;

@Data
public class User {

    private String username;

    private String password;

    private LocalDate birthDate;
}
